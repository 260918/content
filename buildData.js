const fs = require('fs');

const itemList = [
  {
    id: '1',
    mtlNo: "QCL0123",
    netWeight: '12,500',
    orderDate: '2022-09-17 13:00:00',
    grade: 'A',
    supplyDate: '2022-09-17 15:00:00',
    locate: 'L012'
  },
  {
    id: '2',
    mtlNo: "QCL0124",
    netWeight: '15,500',
    orderDate: '2022-09-18 13:00:00',
    grade: 'A',
    supplyDate: '2022-09-18 15:00:00',
    locate: 'L013'
  },
  {
    id: '3',
    mtlNo: "QCL0125",
    netWeight: '11,500',
    orderDate: '2022-09-19 13:00:00',
    grade: 'A',
    supplyDate: '2022-09-19 15:00:00',
    locate: 'L014'
  },
  {
    id: '4',
    mtlNo: "QCL0126",
    netWeight: '16,500',
    orderDate: '2022-09-20 13:00:00',
    grade: 'A',
    supplyDate: '2022-09-20 15:00:00',
    locate: 'L015'
  },
  {
    id: '5',
    mtlNo: "QCL0127",
    netWeight: '17,500',
    orderDate: '2022-09-21 13:00:00',
    grade: 'A',
    supplyDate: '2022-09-21 15:00:00',
    locate: 'L016'
  },
  {
    id: '6',
    mtlNo: "QCL0128",
    netWeight: '12,500',
    orderDate: '2022-09-22 13:00:00',
    grade: 'A',
    supplyDate: '2022-09-122 15:00:00',
    locate: 'L017'
  },
  {
    id: '7',
    mtlNo: "QCL0129",
    netWeight: '13,500',
    orderDate: '2022-09-23 13:00:00',
    grade: 'A',
    supplyDate: '2022-09-23 15:00:00',
    locate: 'L018'
  }
];

fs.writeFileSync('./content/itemList.json', JSON.stringify(itemList));
try {
  fs.mkdirSync('./content/itemList');
} catch (e) {
  if (e.code !== 'EEXIST') throw e;
}
itemList.forEach(item => {
  fs.writeFileSync(
    `./content/itemList/${item.id}.json`,
    JSON.stringify(item),
  );
});
